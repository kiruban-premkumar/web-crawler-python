#!/usr/local/bin/python3.4
# -*- coding: utf-8 -*-

def distance(string,mot1,mot2):
    i = 0
    mots = string.split()
    for m in mots:
        m = m.lower()
        if m == mot1.lower():
            i = 0
        elif m == mot2.lower():
            return i
        else:
            i += 1
    return 0



data = "Ruby est un langage de programmation libre. Il est interprété, orienté objet et multi-paradigme. Le langage a été standardisé au Japon en 2011 (JIS X 3017:2011)1, et en 2012 par l'Organisation internationale de normalisation (ISO 30170:2012)2."

print(distance(data,"ruby","langage"))