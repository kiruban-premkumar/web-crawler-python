#!/usr/local/bin/python3.4
# -*-coding:utf-8 -*

from html.parser import HTMLParser
from urllib.request import urlopen
from urllib import parse
from bs4 import BeautifulSoup
import re
import json

def getWeight(dist):
    if dist < 0:
        return 0
    elif dist <= 5:
        return 10
    elif dist <= 10:
        return 9
    elif dist <= 15:
        return 8
    elif dist <= 20:
        return 7
    elif dist <= 25:
        return 6
    elif dist <= 30:
        return 5
    elif dist <= 35:
        return 4
    elif dist <= 40:
        return 3
    elif dist <= 45:
        return 2
    elif dist <= 50:
        return 1
    else:
        return 0

def distance(string,mot1,mot2):
    i = 0
    mots = string.split()
    for m in mots:
        m = m.lower()
        if m == mot1.lower():
            i = 0
        elif m == mot2.lower():
        	return i
        else:
            i += 1
    return -1

def count_occ(string,mtype):
    i = 0
    mots = string.split()
    for m in mots:
        m = m.lower()
        if m == mtype.lower():
            i += 1
    return i

def html_to_text(data, tag, attr, findAllTags):   
    soup = BeautifulSoup(data)

	#	If there is more than one tag to find
    if findAllTags:
        data = ""
        ListData = soup.findAll(tag, attrs=attr)
        for elem in ListData:
            data += elem.get_text()
    else:
        data = soup.find(tag, attrs=attr).get_text()
    
    return data

def getDataFromUrl(url):
	response = urlopen(url)
	htmlBytes = response.read()
	htmlString = htmlBytes.decode("utf-8")
	return htmlString

def spider(urls, word1, word2):
	foundWord1 = False
	foundWord2 = False
	i = 0
	globalWeight = 0

	#	Crawl through each url
	for url in urls:
		try:
			data = getDataFromUrl(url)

			if i == 0:	#	Wikipédia
				print("Visiting Wikipedia ...")
				clean_data = html_to_text(data, "div", {"class":"mw-body-content"}, False)
			elif i == 1:	#	WordNet
				print("Visiting WordNet ...")
				clean_data = html_to_text(data, "ul", {}, False)
			elif i == 2:	#	Omnilexica
				print("Visiting Omnilexica ...")
				clean_data = html_to_text(data, "ol", {}, True)
			elif i == 3:	#	Google
				print("Visiting Google ...")
				json_data = json.loads(data)
				data = '<ol>' + json_data['responseData']['results'][0]['content'] + ' ' + json_data['responseData']['results'][1]['content'] + ' ' + json_data['responseData']['results'][2]['content'] + ' ' + json_data['responseData']['results'][3]['content'] + '</ol>'
				clean_data = html_to_text(data, "ol", {}, True)

			dist = distance(clean_data,word1,word2)

			tmp_weight = getWeight(dist)
			print("Weight for this search : ", tmp_weight, "/ 10")
			globalWeight += tmp_weight

			"""if dist == -1:
				print("Pas de correspondance entre", word1, "et", word2, "!")
			else:
				print(dist, "words between ", word1, "and", word2)
				if dist > 100 or dist == 0:
					print(word1, "is not of type", word2)
				else:
					print(word1, "is of type", word2)"""

		except:
			print("Failed visiting ", url)

		i += 1	#	END FOR

	#	Calculate the estimation
	length = len(urls)
	maxWeight = 10 * length
	perc = (globalWeight * 100) / maxWeight

	#	Print the result
	print("\nResult:\nThere is ", perc, "% chance for the term \"", word1, "\" to be of type \"", word2, "\".")

kterme = input("Enter a term : ").strip()

ktype = input("Enter the type : ").strip()

print("Check if the term \"", kterme, "\" is of type \"", ktype, "\"\n\n")

urls = ["http://en.wikipedia.org/wiki/"+kterme,
		"http://wordnetweb.princeton.edu/perl/webwn?s="+kterme+"&sub=Search+WordNet&o2=&o0=1&o8=1&o1=1&o7=&o5=&o9=&o6=&o3=&o4=&h=000",
		"http://www.omnilexica.com/?q="+kterme,
		"http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q="+kterme
		]
spider(urls, kterme, ktype)
