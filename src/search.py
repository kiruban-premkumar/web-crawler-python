#!/usr/local/bin/python3.4
# -*- coding: utf-8 -*-

import os
import urllib.request

wiki = False
google = False
distance = 0
distance_max = 100

cwd = os.getcwd()

kterme = input('Entrez un terme : ').strip()

ktype = input('Entrez le type à vérifier : ').strip()

print("Vérifions que le terme", kterme, "est du type", ktype)

# Récupération depuis wikipedia

# if !os.path.isfile(cwd + "/datas/wikipedia-"+kterme+ '.html'):

g = urllib.request.urlopen("http://fr.wikipedia.org/wiki/" + kterme) 
with open(cwd + "/datas/wikipedia-"+kterme+ '.html', 'w+b') as f:
	f.write(g.read())

# Récupération depuis google
g = urllib.request.urlopen("http://www.google.fr/#q=" + kterme) 
with open(cwd + "/datas/google-"+kterme+ '.html', 'w+b') as f:
	f.write(g.read())

f = open( cwd + "/datas/wikipedia-"+kterme+ '.html')

data = f.readlines().join()


print(data)